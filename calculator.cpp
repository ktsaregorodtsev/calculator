#include "calculator.h"

#include <iostream>
#include <cmath>

/**
 * @brief Функция-таблица приоритетов операций
 * @param c Символ
 * @return Приоритет для символов, если символа нет в таблице, то -1
 */
int priorety(char c)
{
    switch (c) {
    case '(':
        return 0;
    case ')':
        return 1;
    case '+':
    case '-':
        return 2;
    case '*':
    case '/':
    case '~':
        return 3;
    case '^':
        return 4;
    }
    return -1;
}


/**
 * @brief Пропускать символ
 * @param c Символ
 * @return Истина, если символ пробел и табуляция
 *
 * @note
 */
bool skipChar(char c)
{
    switch(c) {
    case ' ':
    case '\t':
        return true;
    default:
        return false;
    }

}


/**
 * @brief Символ в число
 * @param c
 * @return число, включая NaN
 *
 * @note Способ с условиями может быть не самый производительный и лаконичный,
 *          но позволяет работать с числами double без сложных действий и позволяет не зависеть от кодировки.
 */
double char2dig(char c)
{
    switch (c) {
    case '0': return 0.0;
    case '1': return 1.0;
    case '2': return 2.0;
    case '3': return 3.0;
    case '4': return 4.0;
    case '5': return 5.0;
    case '6': return 6.0;
    case '7': return 7.0;
    case '8': return 8.0;
    case '9': return 9.0;
    }
    return std::nan("1");
}


bool MakeDigit::addAnyChar(char c)
{
    if(c == '.') {
        m_fracCount = 1;
        return true;
    }
    double v = char2dig(c);
    if(std::isnan(v))
        return false;
    if(!m_fracCount)
        m_int = m_int * 10 + v;
    else {
        m_franc = m_franc * 10 + v;
        m_fracCount++;
    }
    m_isValid = true;
    return true;
}

MakeDigit::operator double() const
{
    return m_int + m_franc / pow(10, m_fracCount - 1);
}

double Calculator::calc(const char *expr)
{
    for(char *it = (char *)expr; *it != '\0'; ++it) {
        // Пропуск пробелов и табуляций
        while(skipChar(*it)) { ++it; }

        MakeDigit d;
        while (d.addAnyChar(*it)) {
            ++it;
        }
        if(d.isValid()) m_workStack.pushBack(d);

        // Пропуск пробелов и табуляций после числа
        while(skipChar(*it)) { ++it; }

        // Проверка на отсутствие операции
        if(*it == '\0') break;

        // Проверка на унарный минус
        char s = *it;
        if(!d.isValid() && *it == '-')
            s = '~';
        // Немного переработанный алгоритм приведения к обратной польской записи
        if(m_storeStack.isEmpty())
            m_storeStack.pushBack(s);
        else if(*it == '(')
            m_storeStack.pushBack(s);
        else if(*it == ')'){
            while(m_storeStack.lastValue() != '('){
                calcOperator(m_storeStack.popBack());
                if(m_storeStack.isEmpty())
                    throw std::runtime_error("Has not opened parentheses");
            }
            m_storeStack.popBack();
        }
        else {
            // Возведение в степень самая приоритетная операция
            // ее приходится отдельно обрабатывать, а точнее пропускать
            while(priorety(m_storeStack.lastValue()) >= priorety(s) && s != '^') {
                calcOperator(m_storeStack.popBack());
                if(m_storeStack.isEmpty()) break;
            }
            m_storeStack.pushBack(s);
        }
    }
    // Производим оставшиеся операции
    while (!m_storeStack.isEmpty()) {
        calcOperator(m_storeStack.popBack());
    }
    return m_workStack.popBack();
}

void Calculator::calcOperator(char c)
{
    double v;
    tryEmpty();
    switch (c) {
    case '~':
        m_workStack.setLastValue(-m_workStack.lastValue());
        break;
    case '+':
        v = m_workStack.popBack();
        tryEmpty();
        m_workStack.setLastValue(m_workStack.lastValue() + v);
        break;
    case '-':
        v = m_workStack.popBack();
        tryEmpty();
        m_workStack.setLastValue(m_workStack.lastValue() - v);
        break;
    case '*':
        v = m_workStack.popBack();
        tryEmpty();
        m_workStack.setLastValue(m_workStack.lastValue() * v);
        break;
    case '/':
        v = m_workStack.popBack();
        tryEmpty();
        m_workStack.setLastValue(m_workStack.lastValue() / v);
        break;
    case '^':
        v = m_workStack.popBack();
        tryEmpty();
        m_workStack.setLastValue(std::pow(m_workStack.lastValue(), v));
        break;
    default:
        // Так же может попасть закрывающая скобка
        throw std::runtime_error("Illegal operator");
    }
}

void Calculator::tryEmpty() const
{
    if(m_workStack.isEmpty())
        throw std::runtime_error("Has not numbers");
}

