cmake_minimum_required(VERSION 3.7)

project(calculator VERSION 0.0.1 LANGUAGES CXX)

set(SOURCES
    calculator.cpp
    items.cpp
    main.cpp)

set(HEADERS
    calculator.h
    container.h
    items.h)

add_executable(${PROJECT_NAME} ${SOURCES} ${HEADERS})

enable_testing()

add_test(NAME full_test COMMAND ${PROJECT_NAME})
