#include "items.h"
#include <cmath>
#include <cstring>
#include <iostream> // std::runtime_error;


template <typename T>
void append(T t1, T t2) { t1 = t2; }


double Minus::calc()
{
    if(m_leftValue == nullptr)
        return -m_rightValue->calc();
    return m_leftValue->calc() - m_rightValue->calc();
}

double Plus::calc()
{
    return m_leftValue->calc() + m_rightValue->calc();
}

double Multiply::calc()
{
    return m_leftValue->calc() * m_rightValue->calc();
}

double Division::calc()
{
    double val = m_rightValue->calc();
    // Проверка деления на ноль
    if(int(val * 1e9) == 0)
        throw std::runtime_error(" /0");
    return m_leftValue->calc() / val;
}

double Pow::calc()
{
    return std::pow(m_leftValue->calc(), m_rightValue->calc());
}

Item::Item(Item *leftValue, Item *rightValue) :
    m_leftValue(leftValue),
    m_rightValue(rightValue)
{

}

Item::Item():
    m_leftValue(nullptr),
    m_rightValue(nullptr)
{

}

void Item::setLeft(Item *leftValue)
{
    m_leftValue = leftValue;
}

void Item::rightLeft(Item *rightValue)
{
    m_rightValue = rightValue;
}

double Number::calc() { return m_value; }
