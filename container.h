#ifndef CONTAINER_H
#define CONTAINER_H

#include <cstdlib>
#include <memory>
#include <stack>

/**
* @brief Си-подобный стек.
*
* @note Контейнер узкоспециализированный можно сильно не прорабатывать и добавить полезные функции
*
*/
template <typename T>
class Stack
{
    struct Item {
        T value;
        Item* previous;
    };

public:
    /**
     * @brief Вспомогательная функция для удаления указателей
     */
    void clear() {
        Item *pointer = m_pointer;
        while(pointer){
            delete pointer->value;
            pointer = pointer->previous;
        }
    }
    ~Stack() {
        while(m_pointer){
            Item *i = m_pointer;
            m_pointer = m_pointer->previous;
            delete i;
        }
    }
    Stack() {}
    // "Дань Великой тройки, чтобы не появилось желание копировать
    Stack(Stack &c) = delete ;
    Stack operator= (Stack &c) = delete ;

    void pushBack(const T &t)
    {
        Item *i = new Item;
        i->value = t;
        i->previous = m_pointer;
        m_pointer = i;
    }

    T popBack() {
        T t = m_pointer->value;
        Item *i = m_pointer;
        m_pointer = m_pointer->previous;
        delete i;
        return t;
    }

    bool isEmpty() const { return m_pointer == nullptr; }

    T lastValue() const { return m_pointer->value; }
    void setLastValue(const T &value) const { m_pointer->value = value; }

private:
    Stack::Item *m_pointer = nullptr;
};

#endif // CONTAINER_H
