#ifndef CALCULATOR_H
#define CALCULATOR_H

#include "container.h"



/**
 * @brief Класс получения числа по мере добавления цифры или точки
 */
class MakeDigit
{
public:
    /**
     * @brief добавить любой символ
     * @param c Символ
     * @return ложь, если символ не является частью числа
     *
     * @note Данная конструкция упрощает обработку при разборе строки
     */
    bool addAnyChar(char c);
    operator double () const;
    bool isValid() const { return m_isValid; }
private:
    double m_int = 0.0; /// <= Целая часть
    double m_franc = 0.0; /// <= Дробная часть
    int m_fracCount = 0.0; /// <= сбор дробной части
    bool m_isValid = false;
};


/**
 * @brief Класс для расчёта выражений
 *
 * @note В данном класе много "Велосипедов", но исключительно из-за условий задачи
 */
class Calculator
{
public:
    /**
     * @brief Функция
     * @param expr
     * @return
     */
    double calc(const char *expr);

private:
    Stack<double> m_workStack; //< Стек для чисел
    Stack<char> m_storeStack; //< Стек для операция

    /**
     * @brief Вспомогательная функция: вызов исключения в случае пустого стека
     */

    void calcOperator(char c);
    /**
     * @brief Вспомогательная функция: вызов исключения в случае пустого стека
     */
    void tryEmpty() const;
};

#endif // CALCULATOR_H
