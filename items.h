#ifndef ITEMS_H
#define ITEMS_H

// Можно использовать такую конструкцию:
#define NO_COPY(X) X(X &) = delete; X operator =(X &) = delete;
// Но несколько снизиться читаемость

/**
 * @brief The items class
 */
class Item
{
public:
    Item(Item *leftValue, Item *rightValue);
    Item();
    virtual double calc() = 0;
    virtual int weight() = 0;
    virtual ~Item() {}
    void setLeft(Item *leftValue);
    void rightLeft(Item *rightValue);

protected:
    Item *m_leftValue;
    Item *m_rightValue;
};

class Number : public Item
{
public:
    NO_COPY(Number)
    Number(double value) : m_value(value) { }
    double calc () override;
    int weight() override { return 4; }

protected:
    double m_value = 0.0;
};

class Minus : public Item
{
public:
    Minus(Item *leftValue, Item *rightValue) : Item(leftValue, rightValue) {}
    NO_COPY(Minus)
    double calc () override;
    int weight() override { return 1; }
};

class Plus : public Item
{
public:
    Plus(Item *leftValue, Item *rightValue) : Item(leftValue, rightValue) {}
    NO_COPY(Plus)
    double calc () override;
    int weight() override { return 1; }

};

class Multiply : public Item
{
public:
    Multiply(Item *leftValue, Item *rightValue) : Item(leftValue, rightValue) {}
    NO_COPY(Multiply)
    double calc () override;
    int weight() override { return 2; }

};

class Division : public Item
{
public:
    Division(Item *leftValue, Item *rightValue) : Item(leftValue, rightValue) {}
    NO_COPY(Division)
    double calc () override;
    int weight() override { return 2; }

};

class Pow: public Item
{
public:
    Pow(Item *leftValue, Item *rightValue) : Item(leftValue, rightValue) {}
    NO_COPY(Pow)
    double calc () override;
    int weight() override { return 3; }
};


#endif // ITEMS_H
