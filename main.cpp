#include <iostream>
#include <cassert>
#include <cmath>

#include "calculator.h"
#include "container.h"

using namespace std;

// Нано-тест-фреймворк
#define test(exp, msg) cout << "Test: " << #exp << endl; assert(((void)msg, exp))
#define double_equal(v1, v2) (fabs((v1) - (v2)) < 1e-12)


int main()
{
    Stack<int *> c;
    c.pushBack(new int(1));
    c.pushBack(new int(3));
    c.pushBack(new int(2));
    c.pushBack(new int(4));

    cout << "Number container";
    test(*c.lastValue() == 4, "Item is 2");
    int *cv = c.popBack();
    test(*cv == 4, "Item is 2");
    test(*c.lastValue() == 2, "Item is 2");
    c.clear();
    cout << " done" << endl;


    MakeDigit m, m1, m2, m3;
    m.addAnyChar('1'); m.addAnyChar('2');m.addAnyChar('3');m.addAnyChar('4');
    m.addAnyChar('.'); m.addAnyChar('1');m.addAnyChar('2');m.addAnyChar('3');
    m1.addAnyChar('1'); m1.addAnyChar('2');m1.addAnyChar('3');m1.addAnyChar('4');
    m2.addAnyChar('0'); m2.addAnyChar('0');m2.addAnyChar('0');m2.addAnyChar('.');m2.addAnyChar('1');
    m3.addAnyChar('.'); m3.addAnyChar('1');

    cout << "Number test";
    test(double_equal(double(m), 1234.123), "Number: 1234.123");
    test(double_equal(double(m1), 1234.0), "Number: 1234");
    test(double_equal(double(m2), 0.1), "Number: 000.1");
    test(double_equal(double(m3), 0.1), "Number: .1");
    cout << " done" << endl;

    cout << "Calculator::calc test";
    test(double_equal(Calculator().calc("10.1"), 10.1), "10.1 = 10.1");
    test(double_equal(Calculator().calc("-(10.1)"), -10.1), "-(10.1) = -10.1");
    test(double_equal(Calculator().calc("2 ^ 2 ^ 3"), 256.0), "2 ^ 2 ^ 3 = 256");
    test(double_equal(Calculator().calc("13.0 / 2."), 6.5), "13.0 / 2. = 6.5");
    test(double_equal(Calculator().calc("((4.0 - 1.) / (2.0 - 1))"), 3.0), "((4.0 - 1.) / (2.0 - 1)) = 3.0");
    test(double_equal(Calculator().calc("(2.0 - 0.5)"), 1.5), "(2.0 - 0.5) = 1.5)");
    cout << " done" << endl;

    return 0;
}
